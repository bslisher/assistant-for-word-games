//
//  SearchTableViewController.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 10/29/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

- (IBAction)orderingBarDidChange;

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet UISegmentedControl *orderingBar;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, strong) IBOutlet UILabel *tableBlocker;

@end
