//
//  Utility.m
//  ScrabbleDictionary
//
//  Created by Brent Slisher on 7/21/13.
//
//

#import "Utility.h"
#import "FMDatabase.h"
#import "AppDelegate.h"

static Utility *sharedInstance;

@implementation Utility {
    NSDictionary *pointValues;
}

+ (Utility *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Utility alloc] init];
    });
    return sharedInstance;
}

- (NSUInteger)pointsForWord:(NSString *)word {
    
    if (pointValues == nil) {
        NSString *pointValuesFileName = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"PointValues.plist"];
        pointValues = [NSDictionary dictionaryWithContentsOfFile:pointValuesFileName];
    }
    
    NSUInteger points = 0;
    for (int i = 0; i < word.length; i++) {
        points += [[pointValues objectForKey:[word substringWithRange:NSMakeRange(i, 1)]] integerValue];
    }
    return points;
}

- (NSArray *)fetchAnagramsOfArrayOfCharacters:(NSArray *)characters {

    NSMutableArray *returnValue = [[NSMutableArray alloc] init];
    
    // Assemble a dictionary of the count of each letter.
    NSMutableDictionary *countOfCharactersInSquares = [[NSMutableDictionary alloc] init];
    for (NSString *character in characters) {
        if (countOfCharactersInSquares[character] == nil) {
            [countOfCharactersInSquares setValue:@1 forKey:character];
        }
        else{
            NSNumber *newValue = @([countOfCharactersInSquares[character] intValue] + 1);
            [countOfCharactersInSquares setValue:newValue forKey:character];
        }
    }
    
    // Start make a SQL statment. The first segment of the WHERE clause will make sure
    // the is an appropriate number of each of the given letters, and the second part will
    // make sure the given letters are the only letters in the word by comparing the sum
    // of the counts of the letters to the word's length. Clear as mud, right?
    NSMutableString *letterCountSegmentOfSQLString = [NSMutableString stringWithString:@""];
    NSMutableString *sumSegmentOfSQLString = [NSMutableString stringWithString:@""];
    __block BOOL firstTimeThroughLoop = YES;
    
    [countOfCharactersInSquares enumerateKeysAndObjectsUsingBlock:^(NSString *letter, NSNumber *count, BOOL *stop) {
        [letterCountSegmentOfSQLString appendFormat:@"%@%@ <= %d", (firstTimeThroughLoop) ? @"" : @" AND ", letter, [count intValue]];
        [sumSegmentOfSQLString appendFormat:@"%@%@", (firstTimeThroughLoop) ? @"" : @"+" , letter];
        firstTimeThroughLoop = NO;
    }];
    
    // Run it...
    FMDatabase *db = [FMDatabase databaseWithPath:[(AppDelegate *)[[UIApplication sharedApplication] delegate] databasePath]];
    [db open];
    NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT word FROM words WHERE (%@) AND (%@) = wordlength", letterCountSegmentOfSQLString, sumSegmentOfSQLString];
    FMResultSet *results = [db executeQuery:sql];
    if (!results) NSLog(@"Error: %@", [db lastErrorMessage]);
    while ([results next]) {
        NSString *word = [results stringForColumn:@"word"];
        [returnValue addObject:@{@"word": word, @"length": @(word.length), @"points": @([[Utility sharedInstance] pointsForWord:word])}];
    }
    [db close];
    
    return returnValue;
    
}

@end
