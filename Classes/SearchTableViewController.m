//
//  SearchTableViewController.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 10/29/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import "SearchTableViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "FMDatabase.h"

@implementation SearchTableViewController {
    NSMutableArray *tableData;
}

- (void)viewDidLoad {
	tableData = [[NSMutableArray alloc] init];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
	_orderingBar.userInteractionEnabled = NO;
	_table.userInteractionEnabled = NO;
	_tableBlocker.hidden = NO;
	[_spinner startAnimating];
	[self performSelectorInBackground:@selector(handleSearchForTerm) withObject:nil];
    [_searchBar resignFirstResponder];
}

- (void)handleSearchForTerm {
	
    [tableData removeAllObjects];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    FMDatabase *db = [FMDatabase databaseWithPath:delegate.databasePath];
    
    [db open];
    NSString *sql = @"SELECT DISTINCT word FROM words WHERE word LIKE ?";
    FMResultSet *results = [db executeQuery:sql, [NSString stringWithFormat:@"%%%@%%", _searchBar.text]];
    if (!results) NSLog(@"Error: %@", [db lastErrorMessage]);
    while ([results next]) {
        NSString *word = [results stringForColumn:@"word"];
        [tableData addObject:@{@"word": word, @"length": @(word.length), @"points": @([[Utility sharedInstance] pointsForWord:word])}];
    }
    [db close];
    
    [self performSelectorOnMainThread:@selector(searchQueryHasCompleted) withObject:nil waitUntilDone:NO];
}

- (void)searchQueryHasCompleted {
    if (tableData.count == 0) {
        NSString *alertMessage = [NSString stringWithFormat:@"No results were found in the dictionary for '%@'.", _searchBar.text];
        [[[UIAlertView alloc] initWithTitle:@"Sorry!" message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else {
        [self orderingBarDidChange];
    }
    
    [_spinner stopAnimating];
    _tableBlocker.hidden = YES;
    _table.userInteractionEnabled = YES;
    _orderingBar.userInteractionEnabled = YES;
}

- (IBAction)orderingBarDidChange {

    NSSortDescriptor *alphabetical = [NSSortDescriptor sortDescriptorWithKey:@"word" ascending:YES];
    NSSortDescriptor *byLength = [NSSortDescriptor sortDescriptorWithKey:@"length" ascending:NO];
    NSSortDescriptor *byPoints = [NSSortDescriptor sortDescriptorWithKey:@"points" ascending:NO];
    
    if (_orderingBar.selectedSegmentIndex == 0) [tableData sortUsingDescriptors:@[alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 1) [tableData sortUsingDescriptors:@[byLength, alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 2) [tableData sortUsingDescriptors:@[byPoints, alphabetical]];
    
    [_table reloadData];
    
}

#pragma mark Search bar methods

- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar {
	theSearchBar.showsCancelButton = NO;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
	theSearchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar resignFirstResponder];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
	
    NSDictionary *dictionaryForWord = tableData[indexPath.row];
	cell.textLabel.text = [dictionaryForWord objectForKey:@"word"];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ points", [dictionaryForWord objectForKey:@"points"]];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate goToDefinitionViewForWord:[[tableData objectAtIndex:indexPath.row] objectForKey:@"word"]];
}

@end

