//
//  AppDelegate.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 10/29/09.
//  Copyright Brent Slisher 2009. All rights reserved.
//

#import "AppDelegate.h"
#import "DefinitionViewController.h"

@implementation AppDelegate {
    NSDictionary *pointValues;
}

- (void)applicationDidFinishLaunching:(UIApplication *)application {

    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = documentPaths[0];
	NSString *const databaseName = @"scrabble.sqlite3";
    _databasePath = [documentsDirectory stringByAppendingPathComponent:databaseName];
    
    // copy database from bundle to documents directory if needed
	if(![[NSFileManager defaultManager] fileExistsAtPath:_databasePath]) {
        NSString *databaseBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
        [[NSFileManager defaultManager] copyItemAtPath:databaseBundlePath toPath:_databasePath error:nil];
    }
    
}

- (void)goToDefinitionViewForWord:(NSString *)word {
    [_tabBarController.viewControllers[0] popToRootViewControllerAnimated:NO];
	_tabBarController.selectedIndex = 0;
	DefinitionViewController *vc = [[DefinitionViewController alloc] initWithNibName:@"DefinitionView" bundle:nil word:word];
	[_tabBarController.viewControllers[0] pushViewController:vc animated:YES];
}

- (void)popDefinitionView {
	[_tabBarController.viewControllers[0] popViewControllerAnimated:YES];
}

@end

