//
//  TestViewController.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 1/12/10.
//  Copyright 2010 Brent Slisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController <UISearchBarDelegate>

- (IBAction)showDefinition;

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UILabel *queryWordInQuotesLabel;
@property (nonatomic, strong) IBOutlet UILabel *isLabel;
@property (nonatomic, strong) IBOutlet UIImageView *goodOrBadLettersImage;
@property (nonatomic, strong) IBOutlet UIButton *definitionButton;
@property (nonatomic, strong) IBOutlet UITextView *directionsTextView;

@end
