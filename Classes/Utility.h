//
//  Utility.h
//  ScrabbleDictionary
//
//  Created by Brent Slisher on 7/21/13.
//
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (Utility *)sharedInstance;

- (NSUInteger)pointsForWord:(NSString *)word;
- (NSArray *)fetchAnagramsOfArrayOfCharacters:(NSArray *)characters;

@end
