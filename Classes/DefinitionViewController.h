//
//  DefinitionViewController.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/3/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefinitionViewController : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil word:(NSString *)word;
- (IBAction)tellDelegateToPopDefinitionView;

@property (nonatomic, strong) IBOutlet UILabel *wordLabel;
@property (nonatomic, strong) IBOutlet UILabel *lettersAndPointsLabel;
@property (nonatomic, strong) IBOutlet UITextView *definitionTextView;

@end
