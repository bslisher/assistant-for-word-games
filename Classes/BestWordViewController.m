//
//  BestWordViewController.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/22/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import "BestWordViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "FMDatabase.h"

@implementation BestWordViewController {
    NSMutableArray *tableData;
}

- (void)viewDidLoad {
	tableData = [[NSMutableArray alloc] init];
}

- (void)performSearch {
    
    if ([[_wordFormat.text stringByReplacingOccurrencesOfString:@"-" withString:@""] length] == 0) {
        // If the whole format string is dashes, just look for anagrams, which is much faster.
        NSMutableArray *characters = [[NSMutableArray alloc] init];
        for (int i = 0; i < _letters.text.length; i++) [characters addObject:[_letters.text substringWithRange:NSMakeRange(i, 1)]];
        tableData = [NSMutableArray arrayWithArray:[[Utility sharedInstance] fetchAnagramsOfArrayOfCharacters:characters]];
    }
    else {
        
        /*
         *  Now I have to construct a horrifying SQL query to find matches.
         */
        
        // FIRST: I will string together a whole bunch of LIKEs to make sure the word matches the given format.

        // I will replace hyphens with underscores, because underscores can be conveniently used in SQLite
        // statements, as documented here: http://sqlite.org/lang_expr.html#like
        
        NSMutableString *formatString = (NSMutableString *)[_wordFormat.text stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
        NSMutableString *formatSegmentOfSQLString = [[NSMutableString alloc] init];
        int originalFormatStringLength = [formatString length];
        
        // If last character is a dash, get rid of dashes at the end.
        if ([[formatString substringFromIndex:([formatString length] - 1)] isEqualToString:@"_"]) {
            // Find the position of the first dash in the last grounp of dashes by starting at the end and iterating backwards.
            int beginningOfLastStringOfDashes = [formatString length] - 1;
            for (; [[formatString substringWithRange:NSMakeRange(beginningOfLastStringOfDashes, 1)] isEqualToString:@"_"]; beginningOfLastStringOfDashes--);
            formatString = (NSMutableString *)[formatString substringToIndex:(beginningOfLastStringOfDashes + 1)];
        }
        
        // Find the end of the first string of dashes.
        int endOfFirstStringOfDashes = 0;
        for (; [[formatString substringWithRange:NSMakeRange(endOfFirstStringOfDashes, 1)] isEqualToString:@"_"]; endOfFirstStringOfDashes++);
        
        // Now we start on the left side of the format string and iterate through the beginning dashes until we hit letters.
        // By comparing the word length against the original format string length, we can assure that the word will fit on
        // the right side as well.
        for (int i = 0; i <= endOfFirstStringOfDashes; i++) {
            [formatSegmentOfSQLString appendFormat:@"%@(word LIKE '%@%%' AND wordlength <= %d)", (i == 0) ? @"" : @" OR ", [formatString substringFromIndex:i], (originalFormatStringLength - i)];
        }
        
        // SECOND: The first part picked out words that fit the format, but we can't be sure those words can be
        // made with the entered letters. The second part of the query will make sure all words are anagrams of
        // the player's letter, plus the letters in the format string.
        
        // Assemble a dictionary of the count of each letter.
        NSString *allLetters = [NSString stringWithFormat:@"%@%@", _letters.text, [_wordFormat.text stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        NSMutableDictionary *sortedLetters = [[NSMutableDictionary alloc] init];
        for (int i = 0; i < allLetters.length; i++) {
            NSString *currentLetter = [[allLetters substringWithRange:NSMakeRange(i, 1)] lowercaseString];
            if (sortedLetters[currentLetter] == nil) {
                [sortedLetters setObject:@1 forKey:currentLetter];
            }
            else{
                NSNumber *newValue = @([sortedLetters[currentLetter] intValue] + 1);
                [sortedLetters setValue:newValue forKey:currentLetter];
            }
        }
        
        // Put the dictionary values into a SQL string
        NSMutableString *letterCountSegmentOfSQLString = [NSMutableString stringWithString:@""];
        NSMutableString *sumSegmentOfSQLString = [NSMutableString stringWithString:@""];
        __block BOOL firstTimeThroughLoop = YES;

        [sortedLetters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSNumber *value, BOOL *stop) {
            [letterCountSegmentOfSQLString appendFormat:@"%@%@ <= %@", (firstTimeThroughLoop) ? @"" : @" AND ", key, value];
            [sumSegmentOfSQLString appendFormat:@"%@%@", (firstTimeThroughLoop) ? @"" : @"+", key];
            firstTimeThroughLoop = NO;
        }];
        
        // Run the whole query...
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        FMDatabase *db = [FMDatabase databaseWithPath:delegate.databasePath];
        [db open];
        NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT word FROM words WHERE (%@) AND (%@) AND (%@) = wordlength", formatSegmentOfSQLString, letterCountSegmentOfSQLString, sumSegmentOfSQLString];
        FMResultSet *results = [db executeQuery:sql];
        if (!results) NSLog(@"Error: %@", [db lastErrorMessage]);
        while ([results next]) {
            NSString *word = [results stringForColumn:@"word"];
            [tableData addObject:@{@"word": word, @"length": @(word.length), @"points": @([[Utility sharedInstance] pointsForWord:word])}];
        }
        [db close];
        
    }
    
    // Back to the main thread...
    [self performSelectorOnMainThread:@selector(searchQueryHasCompleted) withObject:nil waitUntilDone:NO];

}

- (void)searchQueryHasCompleted {
    
    _darkShield.hidden = YES;
    [_spinner stopAnimating];
    _spinner.hidden = YES;
        
    if ([tableData count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"No matches were found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else {
        [self orderingBarDidChange];
    }
    
    _table.userInteractionEnabled = YES;
    _letters.userInteractionEnabled = YES;
    _wordFormat.userInteractionEnabled = YES;
    
}

- (IBAction)orderingBarDidChange {
    
    NSSortDescriptor *alphabetical = [NSSortDescriptor sortDescriptorWithKey:@"word" ascending:YES];
    NSSortDescriptor *byLength = [NSSortDescriptor sortDescriptorWithKey:@"length" ascending:NO];
    NSSortDescriptor *byPoints = [NSSortDescriptor sortDescriptorWithKey:@"points" ascending:NO];
    
    if (_orderingBar.selectedSegmentIndex == 0) [tableData sortUsingDescriptors:@[alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 1) [tableData sortUsingDescriptors:@[byLength, alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 2) [tableData sortUsingDescriptors:@[byPoints, alphabetical]];
    
    [_table reloadData];
    
}

#pragma mark Text field methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
	if (string.length == 0) {
        return YES;
    }
	else if ([textField isEqual:_wordFormat]) {
        NSMutableCharacterSet *alphabetPlusDash = [NSMutableCharacterSet characterSetWithCharactersInString:@"-"];
        [alphabetPlusDash formUnionWithCharacterSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        return (textField.text.length < 15 && [string rangeOfCharacterFromSet:[alphabetPlusDash invertedSet]].location == NSNotFound);
    }
	else {
        NSCharacterSet *alphabet = [NSCharacterSet uppercaseLetterCharacterSet];
        return (textField.text.length < 7 && [string rangeOfCharacterFromSet:[alphabet invertedSet]].location == NSNotFound);
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSString *message = @"";
    
	if (_letters.text.length == 0) {
		message = @"To search for a word that fits, type the letters on your tiles in the letters box.";
    }
	else if (_wordFormat.text.length == 0) {
		message = @"To search for a word that fits, describe the available spaces in the word format box.";
	}
	else {
		[textField resignFirstResponder];
        [tableData removeAllObjects];
		
        _table.userInteractionEnabled = NO;
        _letters.userInteractionEnabled = NO;
        _wordFormat.userInteractionEnabled = NO;
		
        _darkShield.hidden = NO;
        [_spinner startAnimating];

        [self performSelectorInBackground:@selector(performSearch) withObject:nil];
		return YES;
	}
    
    [[[UIAlertView alloc] initWithTitle:@"Sorry!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    return NO;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
	NSDictionary *wordDictionary = [tableData objectAtIndex:indexPath.row];
	cell.textLabel.text = [wordDictionary objectForKey:@"word"];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%d points", [[Utility sharedInstance] pointsForWord:cell.textLabel.text]];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate goToDefinitionViewForWord:[tableData[indexPath.row] objectForKey:@"word"]];
}

@end

