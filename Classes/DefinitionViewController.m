//
//  DefinitionViewController.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/3/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import "DefinitionViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@implementation DefinitionViewController {
    NSString *word;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil word:(NSString *)w {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        word = w;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	    
	_wordLabel.text = word;
	_lettersAndPointsLabel.text = [NSString stringWithFormat:@"%d letters, %d points", word.length, [[Utility sharedInstance] pointsForWord:word]];
    
    //get definition from the database
    FMDatabase *db = [FMDatabase databaseWithPath:[(AppDelegate *)[[UIApplication sharedApplication] delegate] databasePath]];
    [db open];
    _definitionTextView.text = [db stringForQuery:@"SELECT definition FROM words WHERE word = ?", word];
    [db close];
}

- (IBAction)tellDelegateToPopDefinitionView {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate popDefinitionView];
}

@end
