//
//  NSString+OccurancesOfSubstring.m
//  ScrabbleDictionary
//
//  Created by Brent Slisher on 7/20/13.
//
//

#import "NSString+OccurancesOfSubstring.h"
#import "AppDelegate.h"

@implementation NSString (OccurancesOfSubstring)

- (NSUInteger)occurancesOfSubstring:(NSString *)string {
    NSMutableString *selfAsMutableString = [NSMutableString stringWithString:self];
    return [selfAsMutableString replaceOccurrencesOfString:string withString:@"" options:NSLiteralSearch range:NSMakeRange(0, self.length)];
}

@end
