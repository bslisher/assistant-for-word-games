//
//  BestWordViewController.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/22/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BestWordViewController : UITableViewController

- (IBAction)orderingBarDidChange;

@property (nonatomic, strong) IBOutlet UITextField *letters;
@property (nonatomic, strong) IBOutlet UITextField *wordFormat;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet UISegmentedControl *orderingBar;
@property (nonatomic, strong) IBOutlet UILabel *darkShield;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *spinner;

@end
