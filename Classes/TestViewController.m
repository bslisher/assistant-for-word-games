//
//  TestViewController.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 1/12/10.
//  Copyright 2010 Brent Slisher. All rights reserved.
//

#import "TestViewController.h"
#import "AppDelegate.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

@implementation TestViewController {
    NSString *searchTerm;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
	searchTerm = [_searchBar.text uppercaseString];
    FMDatabase *db = [FMDatabase databaseWithPath:[(AppDelegate *)[[UIApplication sharedApplication] delegate] databasePath]];
    BOOL match = NO;
    
    [_searchBar resignFirstResponder];
    
    [db open];
    match = [db boolForQuery:@"SELECT COUNT(word) FROM words WHERE word = ?", searchTerm];
    [db close];
    
    _goodOrBadLettersImage.image = [UIImage imageNamed:match ? @"good.png" : @"bad.png"];
    _definitionButton.hidden = !match;

	_searchBar.text = @"";
	_queryWordInQuotesLabel.text = [NSString stringWithFormat:@"\"%@\"", searchTerm];
	_isLabel.hidden = NO;
	_directionsTextView.hidden = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar {
	theSearchBar.showsCancelButton = NO;
} 

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
	theSearchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar resignFirstResponder];
}

- (IBAction)showDefinition {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate goToDefinitionViewForWord:searchTerm];
}

@end
