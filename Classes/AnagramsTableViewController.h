//
//  AnagramsTableViewController.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/6/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnagramsTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

- (IBAction)orderingBarDidChange;

@property (nonatomic, strong) IBOutlet UITextField *square1;
@property (nonatomic, strong) IBOutlet UITextField *square2;
@property (nonatomic, strong) IBOutlet UITextField *square3;
@property (nonatomic, strong) IBOutlet UITextField *square4;
@property (nonatomic, strong) IBOutlet UITextField *square5;
@property (nonatomic, strong) IBOutlet UITextField *square6;
@property (nonatomic, strong) IBOutlet UITextField *square7;
@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet UISegmentedControl *orderingBar;
@property (nonatomic, strong) IBOutlet UILabel *darkShield;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *spinner;

@end
