//
//  AnagramsTableViewController.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 11/6/09.
//  Copyright 2009 Brent Slisher. All rights reserved.
//

#import "AnagramsTableViewController.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "FMDatabase.h"

@implementation AnagramsTableViewController {
    NSArray *squares;
    NSMutableArray *tableData;
}

- (void)viewDidLoad {
    squares = [NSArray arrayWithObjects:_square1, _square2, _square3, _square4, _square5, _square6, _square7, nil];
    tableData = [[NSMutableArray alloc] init];
}

- (void)performSearch {

    // Fetch anagrams
    NSMutableArray *characters = [[NSMutableArray alloc] init];
    for (UITextField *square in squares) [characters addObject:square.text];
    tableData = [NSMutableArray arrayWithArray:[[Utility sharedInstance] fetchAnagramsOfArrayOfCharacters:characters]];

    // Back to the main thread...
    [self performSelectorOnMainThread:@selector(searchQueryHasCompleted) withObject:nil waitUntilDone:NO];

}

- (void)searchQueryHasCompleted {
    
    if (tableData.count == 0) {
        [[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"No anagrams were found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else {
        [self orderingBarDidChange];
    }
    
    for (UITextField *square in squares) square.userInteractionEnabled = YES;
    
    _table.userInteractionEnabled = YES;
    _darkShield.hidden = YES;
    _spinner.hidden = YES;
    
}

- (IBAction)orderingBarDidChange {
    
    NSSortDescriptor *alphabetical = [NSSortDescriptor sortDescriptorWithKey:@"word" ascending:YES];
    NSSortDescriptor *byLength = [NSSortDescriptor sortDescriptorWithKey:@"length" ascending:NO];
    NSSortDescriptor *byPoints = [NSSortDescriptor sortDescriptorWithKey:@"points" ascending:NO];
    
    if (_orderingBar.selectedSegmentIndex == 0) [tableData sortUsingDescriptors:@[alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 1) [tableData sortUsingDescriptors:@[byLength, alphabetical]];
    else if (_orderingBar.selectedSegmentIndex == 2) [tableData sortUsingDescriptors:@[byPoints, alphabetical]];
    
    [_table reloadData];
    
}

#pragma mark Text field methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet *badCharacters = [[NSCharacterSet uppercaseLetterCharacterSet] invertedSet];
    return (textField.text.length <= 0 && [string rangeOfCharacterFromSet:badCharacters].location == NSNotFound);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	tableData = [[NSMutableArray alloc] init];
    
	[textField resignFirstResponder];
	
    for (UITextField *square in squares) square.userInteractionEnabled = NO;
	
	_table.userInteractionEnabled = NO;
	[tableData removeAllObjects];
	[_table reloadData];
	
    
    for (UITextField *square in squares) {
         if (square.text.length == 0) square.text = @"-";
	}
    
	_darkShield.hidden = NO;
	[_spinner startAnimating];
	_spinner.hidden = NO;
	      
	[self performSelectorInBackground:@selector(performSearch) withObject:nil];
	
	return NO;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
	NSDictionary *wordDictionary = [tableData objectAtIndex:indexPath.row];
	cell.textLabel.text = [wordDictionary objectForKey:@"word"];
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%d points", [[Utility sharedInstance] pointsForWord:cell.textLabel.text]];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate goToDefinitionViewForWord:tableData[indexPath.row][@"word"]];
}

@end

