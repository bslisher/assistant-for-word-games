//
//  AppDelegate.h
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 10/29/09.
//  Copyright Brent Slisher 2009. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate>

- (void)goToDefinitionViewForWord:(NSString *)word;
- (void)popDefinitionView;

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;
//@property (nonatomic, strong) IBOutlet DefinitionViewController *definitionViewController;

@property (nonatomic, strong) NSString *databasePath;

@end
