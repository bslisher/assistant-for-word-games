//
//  NSString+OccurancesOfSubstring.h
//  ScrabbleDictionary
//
//  Created by Brent Slisher on 7/20/13.
//
//

#import <Foundation/Foundation.h>

@interface NSString (OccurancesOfSubstring)

- (NSUInteger)occurancesOfSubstring:(NSString *)string;
    
@end
