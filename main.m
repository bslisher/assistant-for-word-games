//
//  main.m
//  Scrabble Dictionary
//
//  Created by Brent Slisher on 10/29/09.
//  Copyright Brent Slisher 2009. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

